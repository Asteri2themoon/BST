#ifndef __TREE_H__
#define __TREE_H__

#define max(a,b) ((a)<(b)?(b):(a))

typedef enum{
	true=1,
	false=0
}bool;

typedef enum{
	Right,
	Left
}Side;

typedef struct node{
	struct node *prev;
	struct node *left;
	struct node *right;
	int leftLen;
	int rightLen;
	char name[16];
	void *value;
}Tree;

//library
Tree* treeAdd(Tree *tree,char *key,void *value);
void* treeFind(Tree *tree,char *key);

//process
Tree* treeCreate(char *name,void *value);
Tree* treeRotate(Tree *tree,Side side);
void treeUpdateHeightFrom(Tree *tree);

//debug
void treeDisplayList(Tree *tree,int level);
bool treeConnectionCheck(Tree *tree);
bool treeHeightCheck(Tree *tree);
bool treeIsWellBalanced(Tree *tree);

#endif