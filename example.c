#include <stdlib.h>
#include <stdio.h>

#include "tree.h"

int main(){
	int var1=3,test2=0,value3=5,variable4=6;
	void* e=NULL;
	Tree *tree=NULL;
	printf("creation de l'arbre:\n");
	tree=treeAdd(tree,"var1",&var1);
	printf("var1=%d\n",var1);
	tree=treeAdd(tree,"test2",&test2);
	printf("test2=%d\n",test2);
	tree=treeAdd(tree,"value3",&value3);
	printf("value3=%d\n",value3);
	tree=treeAdd(tree,"variable4",&variable4);
	printf("variable4=%d\n",variable4);
	printf("\nrecherche:\n");
	e=treeFind(tree,"var1");
	if(e!=NULL){
		printf("var1=%d\n",*((int*)e));
	}else{
		printf("impossible de trouver var1\n");
	}
	e=treeFind(tree,"value3");
	if(e!=NULL){
		printf("value3=%d\n",*((int*)e));
	}else{
		printf("impossible de trouver value3\n");
	}
	e=treeFind(tree,"test4");
	if(e!=NULL){
		printf("test4=%d\n",*((int*)e));
	}else{
		printf("impossible de trouver test4\n");
	}
	return 0;
}