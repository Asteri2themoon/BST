#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "tree.h"

Tree* treeCreate(char* name,void* value){
	Tree *tree=(Tree*)malloc(sizeof(Tree));
	tree->prev=NULL;
	tree->left=NULL;
	tree->right=NULL;
	tree->leftLen=0;
	tree->rightLen=0;
	stpcpy(&tree->name[0],name);
	tree->value=value;
	return tree;
}

Tree* treeRotate(Tree* tree,Side side){
	Tree* prev=tree->prev,*new;
	if(side==Right){//right rotate
		new=tree->left;//replace current node by left child
		new->prev=prev;
		if(prev!=NULL){
			if(prev->right==tree){
				prev->right=new;
			}else{
				prev->left=new;
			}
		}
		tree->left=new->right;//replace left child by the right child of the new node
		if(new->right!=NULL){
			new->right->prev=tree;
		}
		new->right=tree;
		tree->prev=new;
		tree->leftLen=new->rightLen;//update width value
		new->rightLen=max(tree->rightLen,tree->leftLen)+1;
		treeUpdateHeightFrom(new);
		return new;
	}//left rotate
	new=tree->right;//replace current node by left child
	new->prev=prev;
	if(prev!=NULL){
		if(prev->left==tree){
			prev->left=new;
		}else{
			prev->right=new;
		}
	}
	tree->right=new->left;//replace left child by the right child of the new node
	if(new->left!=NULL){
		new->left->prev=tree;
	}
	new->left=tree;
	tree->prev=new;
	tree->rightLen=new->leftLen;//update width value
	new->leftLen=max(tree->rightLen,tree->leftLen)+1;
	treeUpdateHeightFrom(new);
	return new;
}

bool treeConnectionCheck(Tree *tree){
	bool status=true;
	if(tree->right!=NULL){
		status=(tree->right->prev==tree)?true:false;
		if(status==true){
			status=treeConnectionCheck(tree->right);
		}
	}
	if(tree->left!=NULL && status==true){
		status=(tree->left->prev==tree)?true:false;
		if(status==true){
			status=treeConnectionCheck(tree->left);
		}
	}
	return status;
}

bool treeHeightCheck(Tree *tree){
	if(tree->right!=NULL){
		if(tree->rightLen!=(max(tree->right->rightLen,tree->right->leftLen)+1)){
			return false;
		}
	}
	if(tree->left!=NULL){
		if(tree->leftLen!=(max(tree->left->rightLen,tree->left->leftLen)+1)){
			return false;
		}
	}
	return (tree->right!=NULL?treeHeightCheck(tree->right):true)==true && (tree->left!=NULL?treeHeightCheck(tree->left):true)==true;
}

bool treeIsWellBalanced(Tree *tree){
	if(abs(tree->rightLen-tree->leftLen)>1){
		return false;
	}
	return (tree->right==NULL?true:treeIsWellBalanced(tree->right))==true && (tree->left==NULL?true:treeIsWellBalanced(tree->left))==true;
}

void treeUpdateHeightFrom(Tree *tree){
	while(tree!=NULL){
		if(tree->left!=NULL){
			tree->leftLen=max(tree->left->rightLen,tree->left->leftLen)+1;
		}
		if(tree->right!=NULL){
			tree->rightLen=max(tree->right->rightLen,tree->right->leftLen)+1;
		}
		tree=tree->prev;
	}
}

Tree* treeAdd(Tree* tree,char* key,void* value){
	if(tree==NULL){
		return treeCreate(key,value);
	}
	bool wellBalanced;
	int compare=-1;
	Tree *node=tree,*balance;
	while(compare!=0){//find and add the new element
		compare=strcmp(key,node->name);
		if(compare>0){
			if(node->right!=NULL){
				node=node->right;
			}else{
				node->right=treeCreate(key,value);
				node->rightLen=1;
				node->right->prev=node;
				compare=0;
			}
		}else{
			if(node->left!=NULL){
				node=node->left;
			}else{
				node->left=treeCreate(key,value);
				node->leftLen=1;
				node->left->prev=node;
				compare=0;
			}
		}
	}
	wellBalanced=true;
	balance=node->prev;
	while(balance!=NULL){
		if(balance->left!=NULL){
			balance->leftLen=max(balance->left->rightLen,balance->left->leftLen)+1;
		}
		if(balance->right!=NULL){
			balance->rightLen=max(balance->right->rightLen,balance->right->leftLen)+1;
		}
		if(abs(balance->rightLen-balance->leftLen)>1){
			wellBalanced=false;
		}
		balance=balance->prev;
	}
	if(wellBalanced==false){//rebalance the tree from the new node
		while(node!=NULL){
			if(abs(node->rightLen-node->leftLen)>1){
				if(node->rightLen>node->leftLen){//right heavy
					if(node->right->leftLen<=node->right->rightLen){
						node=treeRotate(node,Left);
					}else{
						treeRotate(node->right,Right);
						node=treeRotate(node,Left);
					}
				}else{//left heavy
					if(node->left->rightLen<=node->left->leftLen){
						node=treeRotate(node,Right);
					}else{
						treeRotate(node->left,Left);
						node=treeRotate(node,Right);
					}
				}
			}
			if(node->prev==NULL){
				tree=node;
			}
			node=node->prev;
		}
	}
	return tree;
}

void* treeFind(Tree *tree,char *key){
	if(tree==NULL){
		return NULL;
	}
	int compare=-1;
	while(compare!=0){//find and add the new element
		compare=strcmp(key,tree->name);
		if(compare==0){
			return tree->value;
		}else if(compare>0){
			if(tree->right!=NULL){
				tree=tree->right;
			}else{
				return NULL;
			}
		}else{
			if(tree->left!=NULL){
				tree=tree->left;
			}else{
				return NULL;
			}
		}
	}
}

void treeDisplayList(Tree *tree,int level){
	if(tree->right!=NULL)
		treeDisplayList(tree->right,level+1);
	for(int i=0;i<level;i++)
		printf("  ");
	printf("-%d/%d %s (0x%06x)\n",tree->rightLen,tree->leftLen,tree->name,tree->value);
	if(tree->left!=NULL)
		treeDisplayList(tree->left,level+1);
}