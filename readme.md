# arbre de recherche binaire �quilibr� (langage C)

Petite bibliot�que pour utiliser des arbre de recherche binaire �quilibr� en C.

# Documentation

### les types
**type bool**
```c
typedef enum{
	true=1,
	false=0
}bool;
```
**type Side**
```c
typedef enum{
	Right,
	Left
}Side;
```
Le type Side permet de d�finir le sense de rotation des noeuds.
**type Tree**
```c
typedef struct node{
	struct node *prev;
	struct node *left;
	struct node *right;
	int leftLen;
	int rightLen;
	char name[16];
	void *value;
}Tree;
```
Structure pour g�rer les arbres de recherche binaire r�cursivement.

### fonctions
```c
Tree* treeAdd(Tree *tree,char *key,void *value)
```
Permet d'ajouter un �lement dans l'arbre.

>**@param tree** arbre dans lequel on veux ajouter l'�l�ment  
>**@param key** cl� de l'�lement � ajouter  
>**@param value** valeur de l'�l�ment � ajouter (sous forme de pointeur)  
>**@return** nouvel arbre  

```c
void* treeFind(Tree *tree,char *key)
```
Permet de rechercher un �l�ment dans un arbre.

>**@param tree** arbre dans lequel on veux chercher  
>**@param key** cl� de l'�lement � rechercher  
>**@return** valeur de l'�l�ment (renvoie `NULL` si il n'existe pas)  

# Exemple
```c
#include <stdlib.h>
#include <stdio.h>

#include "tree.h"

int main(){
	int var1=3,test2=0,value3=5,variable4=6;
	void* e=NULL;
	Tree *tree=NULL;
	printf("creation de l'arbre:\n");
	tree=treeAdd(tree,"var1",&var1);
	printf("var1=%d\n",var1);
	tree=treeAdd(tree,"test2",&test2);
	printf("test2=%d\n",test2);
	tree=treeAdd(tree,"value3",&value3);
	printf("value3=%d\n",value3);
	tree=treeAdd(tree,"variable4",&variable4);
	printf("variable4=%d\n",variable4);
	printf("\nrecherche:\n");
	e=treeFind(tree,"var1");
	if(e!=NULL){
		printf("var1=%d\n",*((int*)e));
	}else{
		printf("impossible de trouver var1\n");
	}
	e=treeFind(tree,"value3");
	if(e!=NULL){
		printf("value3=%d\n",*((int*)e));
	}else{
		printf("impossible de trouver value3\n");
	}
	e=treeFind(tree,"test4");
	if(e!=NULL){
		printf("test4=%d\n",*((int*)e));
	}else{
		printf("impossible de trouver test4\n");
	}
	return 0;
}
```
